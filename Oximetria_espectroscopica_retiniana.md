---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.11.5
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

# Oximetria Retiniana


## Introducción

Según la Organización Mundial de la Salud, las enfermedades de la retina afectan a  millones de personas en el mundo. Por ejemplo, en el año 2012  degeneración macular relacionada con la edad  y el glaucoma afectaron a más de 5.4 millones de casos [1]. La  captación de  imágenes de fondo de ojo suministra un método esencial  y muy valioso en el diagnóstico de estas importantes patologías asociadas con el sistema vascular retiniano tales como la retinopatía diabética, la degeneración macular o el glaucoma, entre otros.  Además, permite visualizar, de forma no invasiva, otras partes de la retina como el nervio óptico, la mácula, fóvea, o la presencia de microaneurismas y exudados [4].  Como muestra, en la Fig.1 se puede ver una serie de imágenes  que muestran diferentes estados patológicos del fondo de ojo. 

<img src="figoxi_1.png" style="max-width:100%" />


Por todo ello, el objetivo principal  de este problema es doble: en primer lugar consiste en diseñar un  prototipo de cámara de fondo de ojo de bajo costo, transportable basado en la alta resolución de las cámaras que incorporan los teléfonos móviles de última generación. Como segundo objetivo, pretendemos extraer datos cuantitativos mediante el procesado de la imagen para determinar el grado de oxigenación de diferentes puntos de la retina. 


+++

##  Oximetría espectroscópica retiniana

El montaje típico se muestra en la figura: una fuente de luz blanca ilumina un mazo de fibras  compuesta de una fibra central que transporta la radiación hasta el tejido y fibras que recogen la radiación reflejada. 

<img src="figoxi_2.png" style="max-width:100%" />


Se obtiene así un espectro de reflectancia $R(\lambda)=I_R(\lambda)/I_0(\lambda)$, donde $I_0(\lambda)$ e $I_R(\lambda)$ representan la intensidad incidente y reflejada difusa, respectivamente. No referimos a esta reflectancia como reflectancia difusa debido a que la luz o radiación que emerge del tejido retiniano  ha experimentado muchos eventos de absorción y scattering por las moléculas y estructuras retinianas. En su camino cada componente de haz incidente se desviará de su trayectoria y será parcial o totalmente absorbido. Los cambios experimentados por las diferentes componentes espectrales de la luz incidente llevarán información del tipo y de la concentración de moléculas en el tejido y de los tamaños de las estructuras que lo componen.

### Problema 5.2c: Oximetría de dos longitudes de onda

La  extracción de parámetros biológicos tales como la concentración de especies existentes en un tejido o  los tamaños de las estructuras del mismo, se basan en la modificación que experimenta un haz de radiación de cierto contenido espectral al propagarse  dentro del tejido hasta llegar al detector. Este detector puede ser un detector de imagen o un espectrofotómetro.  En cualquier caso, el haz incidente sobre el tejido (en nuestro caso la retina) se modifica debido, básicamente a la absorción por lo cromóforos del tejido y al scattering inducido por las estructuras del mismo y las variaciones de los índices de refracción. La oximetría consiste en la determinación de las concentraciones de hemoglobina y oxihemoglobina. Más concretamente, el parámetro de oxígeno en sangre se define como 

$$S = \frac{C_O}{C_H + C_O}$$

donde $C_H$ y $C_O$ representan las concentraciones de hemoglobina y oxihemoglobina, respectivamente.  Para la determinación no invasiva de estas concentraciones deberemos definir un modelo  de absorción  y de scattering  del tejido en cuestión. 

**Enunciado Problema 5.2c**

*Se quiere conocer el porcentaje de oxihemoglobina de los capilares retinianos. Para ello se usa un método espectroscópico como el de la figura. Se supone que los únicos cromóforos en los capilares retinianos son la hemoglobina y la oxihemoglobina cuyos coeficientes de absorción molar se conocen. *

<img src="figoxi_3.png" style="max-width:100%" />


*Se mide el espectro de la luz retroreflejada y se obtiene la absorbancia de la figura. A partir de dos valores de la absorbancia para dos longitudes de onda se puede obtener el parámetro de oxígeno. Plantee las ecuaciones necesarias y obtenga el valor del parámetro de saturación de oxígeno en sangre, SO2, usando $\lambda_1=$ 500 nm y $\lambda_2$= 600 nm*.

Los coeficientes de absorción molar de la hemoglobina y la oxihemoglobina se dan en la figura siguiente:

<img src="figoxi_4.png" style="max-width:100%" />

**Solucion**:

Respecto de la absorción, el modelo de Lambert-Beer establece que la absorbancia se puede poner como una combinación lineal de las absorbancias  parciales de cada componente

$$A(\lambda_j) = \sum C_j \varepsilon_m^{(j)} d_j$$

<img src="figoxi_5.png" style="max-width:100%" />

donde $C_j$ es la concentración del j-cromóforo, $\varepsilon_m^{(j)}(\lambda)$ su coeficiente de absorción molar (cm-1M-1), y $d_j$ el espesor del tejido. En el caso de ojo, consideraremos que los dos cromóforos más importantes para nuestro estudio son la hemoglobina y la oxihemoglobina, cuyos coeficientes de absorción molar se muestran en la figura de más arriba. Como se puede observar, ambas absorben fuertemente en el azul. 
Si  ignoramos el scattering,  y medimos la absorbancia para dos longitudes de onda diferentes, $\lambda_1$ y $\lambda_2$,  (normalmente  570 nm (isobéstico) y 600 nm) , obtendremos para cada longitud de onda,

$$A(\lambda_1) = ( C_H \varepsilon_m^H(\lambda_1) + C_O \varepsilon_m^O (\lambda_1)) d$$
$$A(\lambda_2) = ( C_H \varepsilon_m^H(\lambda_2) + C_O \varepsilon_m^O (\lambda_2)) d$$

Si definimos el cociente  $R= A(\lambda_1)/A(\lambda_2)$, resolviendo el sistema, se llega sin dificultad a que,

$$SO2 = \frac{C_O}{C_H + C_O} = \frac{\varepsilon_m^H(\lambda_1) - R \varepsilon_m^H(\lambda_2)}{R [\varepsilon_m^O(\lambda_2) - \varepsilon_m^H(\lambda_2) ] + [\varepsilon_m^H(\lambda_1) - \varepsilon_m^O(\lambda_1) ] }$$

La ecuación anterior nos indica que si podemos medir el cocicnte de absorbancias para dos longitudes de onda y conocemos los valores del coeficiente de absorcion mola para esas longitudes de onda, se puede obtener el valos de SO2. En nuestro caso, no hay más que sustituir los valores que se pueden obtener de las gráficas suministradas paras las longitudes de onda indicadas: 

$$\varepsilon_m^H ( \lambda_1 = 500 nm) \simeq 2 \times 10^4 cm^{-1} M^{-1}$$
$$\varepsilon_m^O ( \lambda_1 = 500 nm) \simeq 1.5 \times 10^4 cm^{-1} M^{-1}$$
$$\varepsilon_m^H ( \lambda_2 = 600 nm) \simeq 3 \times 10^3 cm^{-1} M^{-1}$$
$$\varepsilon_m^O ( \lambda_2 = 600 nm) \simeq 3 \times 10^3 cm^{-1} M^{-1}$$

Por otra parte, el cociente de absorbancias se obtiene de la gráfica $R = \frac{A(\lambda_1)}{A(\lambda_2)} = \frac{0.1}{0.04} = 2.5$.  Por lo que, finalmente, el resultado es,

$$SO2 = \frac{2 \times 10^4 - 2.5 \times 1.5 \times 10^4}{2.5  \times [3 \times 10^3 - 1.5 \times 10^4] + [2 \times 10^4 - 2 \times 10^4]} = 0.68$$

Este método, aunque sencillo de aplicar, presenta sin embargo algunos problemas. Uno de ellos es que ignora la influencia del scattering en los medios oculares y retinianos, lo que conduce a una sobre-estimación
de los valores de SO2.


+++

### Problema 5.2d: Oximetría espectral

Podemos mejorar el cálculo de la oximetría retiniana si introducimos en nuestro modelo de reflectancia el scattering que tiene lugar en la retina y además extendemos nuestro análisis a más longitudes de onda del
espectro experimental obtenido. Este es el objeto del problema 5.2d.

**Enunciado**

*Se quiere medir el oxigeno en sangre de estructuras retinianas. Para ello se emplea una cámara de fondo de ojo modificada, a la cual se ha incorporado un espectrómetro, tal como se indica en la figura.
La luz blanca se dirige sobre el ojo y la luz retroreflejada por todo el sistema se analiza mediante un espectrofotómetro.*

<img src="figoxi_6.png" style="max-width:100%" />

*a) Explicar brevemente cómo funciona el dispositivo óptico y qué es lo que mide.*

La luz blanca procedente de una fuente halógena, ilumina el fondo del ojo mediante un sistema de lentes adecuado. La luz retroreflejada por la retina, emerge del ojo y se enfoca en la rendija de un espectrómetro
que analiza el espectro de esta radiación. Eventualmente, parte del haz emergente se visualiza mediante una cámara para saber el punto de la retina que estamos analizando.

*(b) Los resultados experimentales de la función Absorbancia $R=-log(I_R/I_0)$ para las venas y arterias retinianas se dan en los ficheros AbsorbanciaVenas.dat y AbsorbanciaArterias.dat. Representar en una gráfica la absorbancia del ojo para los tres casos, en función de la longitud de onda. Comentar brevemente el resultado.*

```{code-cell} ipython3
import numpy as np
import matplotlib.pyplot as plt
###CARGA DE FICHEROS DE ESPECTROS DE LOS CROMOFOROS RELEVANTES ##
AbsA = np.loadtxt('AbsorbanciaArterias.dat') #Carga del fichero experimental
landa1 = AbsA[:,0] #Longitudes de onda del espectro.
A_ojo1=AbsA[:,1] #A_ojo=log10(R_ojo);
AbsV = np.loadtxt('AbsorbanciaVenas.dat') #Carga del fichero experimental.
landa2 = AbsV[:,0] #Longitudes de onda del espectro.
A_ojo2=AbsV[:,1] 
fig = plt.figure(figsize=(8,6))
plt.plot(landa1,A_ojo1,'bo',linewidth=2)
plt.plot(landa2,A_ojo2,'ro',linewidth=2)
plt.xlabel('Longitud de onda [nm]',fontsize=12)
plt.ylabel(r'A$_{ojo}$',fontsize=12)
plt.title('Absortancia del ojo',fontsize=16)
plt.legend(('Arterias', 'Venas'),fontsize=12)
```

Se puede observar que la absorbancia presenta en ambos casos dos máximos que corresponden a los
máximos de absorción de la oxihemoglobina. La venas transportan una mayor cantidad de sangre y tienen
mayor espesor, por lo que la absorbancia es mayor para ellas.


*(c) Se quiere medir el grado de oxigenación en la zona de arterias retinianas y en la zona del nervio óptico. Para ajustar los datos obtenidos se necesita un modelo teórico de la absorbancia en el ojo. El modelo teórico debe incorporar las características espectrales de la hemoglobina y la oxihemoglobina.
Aplique la ley de Beer para obtener el modelo.*

La presencia de glóbulos rojos, cuyo tamaño está entre 5 a 10 micras, y a las variaciones de índice de refracción entre estos y el plasma, con índices de n= 1.406 y 1.345, respectivamente, hacen que la retina se comporte como un medio de alto scattering, principalmente scattering Mie. De una manera general, la reflectancia de de la retina se podrá poner como:

$$R = \frac{I_R(\lambda)}{I_0} = 10^{-2[A_{Hb}(\lambda) + A_{HbO2}(\lambda) + A_{scatt}(\lambda)]} R_s$$

donde $A_j(\lambda) = C_j \varepsilon_m^{(j)(\lambda) L}$ representa la absortancia de cada cromóforo, $A_{scatt}$ la contribución del scattering y $R_s(\lambda)$ la reflectancia de la esclera, la cual se asumirá constante.

La absorbancia se podrá poner como $A = - log(R)$, es decir, 

$$A = - log(R\lambda) = -log(\frac{I_R(\lambda)}{I_0} ) = 2[A_{Hb}(\lambda) + A_{HbO2}(\lambda) + A_{scatt}(\lambda)] + log( R_s ) $$

o bien, 

$$A = [2 C_{Hb}\varepsilon_m^{(Hb)(\lambda) + 2 C_{ox} \varepsilon_m^{(ox)(\lambda) + 2 A_{scatt}] L + L log(R_s)$$

Si incluimos el producto $2C_i L= m_i$ en una constante desconocida para cada sustancia ($m_i$), y asumimos que en el ojo el scattering es de tipo Mie, podemos suponer que la parte de la absorbancia debida al scattering ese
puede poner como $m_{scatt}(\lambda)$. Con todo, el modelo de reflectancia se puede poner como, 

$$A  = m_{Hb} \varepsilon_m^{(Hb)(\lambda) + m_{ox} \varepsilon_m^{(ox)(\lambda) + m_{scatt}(\lambda) + m_s$$

donde las constantes $m_j$ son desconocidas y proporcionales a las concentraciones de cada cromóforo.

*(d) Se quieren obtener los porcentajes relativos de hemoglobina, y en el ojo. Para ello se tiene que realizar un ajuste entre un modelo teórico y los resultados experimentales obtenidos. Los ficheros de los coeficientes de absorción molar de la hemoglobina, oxihemoglobina se dan en los ficheros CoefExtincionHemo.dat, CoefExtincionOXIhemo.dat.*

Ahora que disponemos de un modelo sobre la absortancia del ojo, podemos llevar a cabo un ajuste entre este modelo teórico y los datos de la absortancia experimental para obtener las constantes $m_j$. El grado de
oxigenación se podrá obtener a través de estos coeficientes $S= m_{ox}/(m_{ox}+m_{Hb})$. Vamos a realizar el script correspondiente:


```{code-cell} ipython3
import numpy as np
import matplotlib.pyplot as plt

# ########################################################################
# El programa ajusta el espectros de reflectancia del fondo del ojo
# a un modelo descrito en la función funcion_AjusteOjo
########CARGA DE FICHEROS DE ESPECTROS DE LOS CROMOFOROS RELEVANTES##########
#----------------Absortancia de arterias retiniales------------------
AbsorbanciaArterias = np.loadtxt('AbsorbanciaArterias.dat')
 #Carga del fichero experimental.
landas_exp = AbsorbanciaArterias[:,0]
 #Longitudes de onda del espectro.
R_ojo=AbsorbanciaArterias[:,1]
#----------------------- Absortancia de venas-----------------------------------------
#np.loadtxt AbsorbanciaVenas.dat
 #Carga del fichero experimental.
#landas_exp = AbsorbanciaVenas[:,0]
 #Longitudes de onda del#espectro.
#R_ojo=AbsorbanciaVenas[:,1]
#-------------------------------------------------------------------------
#--------------------------------------------------------------------------#
# CARGA LOS ESPECTROS DE ARCHIVO DE SANGRE Y AGUA
 #
#--------------------------------------------------------------------------#
A=np.loadtxt('epsilon_mOxy.dat')
B=np.loadtxt('epsilon_mHemo.dat')
nmSG= A[:,0] #carga las longitudes de onda en nm
epsilonoxySG = A[:,1] #coeficiente de absorcion molar de la oxihemoglobina
epsilondeoxySG = B[:,1] #coeficiente de absorcion molar de la Hemoglobina
norm1=max(epsilonoxySG)
norm2=max(epsilondeoxySG)
MMM=[norm1,norm2] #Calcula el máximo de cada espectro.
norm=max(MMM) #Calcula el máximo de entre los máximos
epsilonoxySG = A[:,1]/norm #coeficiente de absorcion molar de la oxihemoglobina normalizepsilondeoxySG = B[:,1]/norm
#--------------------------------------------------------------------------
#
# AJUSTE DE LOS DATOS
# #
#--------------------------------------------------------------------------

def funcionError(start, landa_exp, R_ojo, nmSG,epsilonoxySG,  epsilondeoxySG):
    #  funcionError es una funcion error entre el modelo y los datos experimentales 
    #  es la función que hay que minimizar mediante el algoritmo fminsearch
    #  A esta función el programa le pasa los datos: (start, nm0, ES, nmSG, muaoxySG, muadeoxySG, nmH2O, muaH2O,u,v)

    #parámetros de ajuste de partida.

    m1=start[0]
    m2= start[1]
    m3 = start[2]
    m4= start[3]

    #nmOff= start[6]

    ###########################################################################
    # MODELO DE ABSORCION DE LA HEMOGLOBINA, OXIHEMOGLOBINA, AGUA Y MELANINA  #
    ###########################################################################
    #ESPECTROS INTERPOLADOS A ''landa_exp''
    epsilonoxy=np.interp(landa_exp,nmSG,epsilonoxySG)
    epsilondeoxy=np.interp(landa_exp,nmSG,epsilondeoxySG)

    R_teorica= m1*epsilondeoxy+ m2*epsilonoxy+ m3*(1/(landa_exp/650))+ m4
    #FUNCION ERROR
    err = np.sum( (R_teorica - R_ojo)**2 )   # funcion error que se quiere minimizar en todo el intervalo espectral

    #if m1<0.0:   
    #    err = err*2  #rebaja las exigencias de precisión al dar S>1 que no puede ser
    #if m2<0.0: 
    #    err = err*2 #rebaja las exigencias de precision al dar S<01 (no puede ser)
    #if m3<0.0: 
    #    err = err*2 #rebaja las exigencias de precisión al dar S<01 (no puede ser)
    return err


#VALORES DE PARTIDA APROXIMADOS PARA REALIZAR EL AJUSTE
m1 =0.005  # Fracción en volumen de hemoglobina
m2 =0.05  # Fracción en volumen de Oxyhemoglobina
m3=0.1  # fraccion de scattering
m4=-0.1  # fondo
start = [m1,m2,m3,m4]

from scipy.optimize import fmin,minimize


resultado = minimize(funcionError,x0=start,args=(landas_exp, R_ojo, nmSG,epsilonoxySG,  epsilondeoxySG), 
                     tol=1e-15,bounds=((0,0.1),(0,np.inf),(0,np.inf),(-np.inf,np.inf)))

# A la función minimize se le pasa 
#1. La función  funcionError
#2. Los valores de partida contenidos en 'start' 
#3. Las opciones de aproximacion. Si no se indica nada toma las opciones por defecto.   


#El resultado del ajuste se encuentra en la variable resultado anterior.
m1=resultado.x[0]
m2 = resultado.x[1]
m3 = resultado.x[2]
m4 = resultado.x[3]
# ESPECTROS INTERPOLADOS A ''nm''
epsilonoxy=np.interp(landas_exp,nmSG,epsilonoxySG)
epsilondeoxy=np.interp(landas_exp,nmSG,epsilondeoxySG)
# Reflectancia teórica
R_teorica=m1*epsilondeoxy+ m2*epsilonoxy+ m3*1/(landas_exp/650)+ m4
                       
fig = plt.figure(figsize=(8,6))
plt.plot(landas_exp, R_ojo, 'ko')
plt.plot(landas_exp, R_teorica, 'r-',linewidth=2)
plt.ylim(0,0.1)
plt.xlabel(r'$\lambda$ (nm)')
plt.ylabel(r'A$_{ojo}$')
x = 500 ;ymax =0.114 ; ds = .02 ; sz=12
plt.text(x, ymax - ds, f'm1 ={m1:e}',fontsize=sz)
plt.text(x, ymax - 2*ds, 'm2 =' + str(m2)[:5],fontsize=sz)
plt.text(x+75, ymax - ds, 'm3 =' + str(m3)[:5],fontsize=sz)
plt.text(x+75, ymax - 2*ds, 'm4 =' + str(m4)[:6],fontsize=sz)
print('Parámetro de oxígeno en sangre SO = ', np.round(m2/(m1+m2),3))
                       
```

```{code-cell} ipython3

```
