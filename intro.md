# Óptica Biomédica 

El presente libro pretende introducir conceptos de Óptica Biomédica así como
algunas de sus aplicaciones mediante ejemplos y ejercicios resueltos que hacen uso
de pequeños códigos en Python. 
Todos el contenido es accesible para su descarga y los códigos pueden ser modificados
para explorar otros casos y profundizar en la materia.


```{tableofcontents}
```
