
 function err =Funcion_ajusteOjo(start, landa_exp, R_ojo, nmSG,epsilonoxySG,  epsilondeoxySG)
 

global cnt  

%  function err = es una funcion error entre el modelo y los datos experimentales 
%  es la función que hay que minimizar mediante el algoritmo fminsearch
%  A esta función el programa le pasa los datos: (start, nm0, ES, nmSG, muaoxySG, muadeoxySG, nmH2O, muaH2O,u,v)



%parámetros de ajuste de partida.

m1=start(1);
m2= start(2);
m3 = start(3);
m4= start(4);



%nmOff= start(7);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MODELO DE ABSORCION DE LA HEMOGLOBINA, OXIHEMOGLOBINA, AGUA Y MELANINA  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%ESPECTROS INTERPOLADOS A ''landa_exp''

  epsilonoxy=interp1(nmSG,epsilonoxySG,landa_exp);
  epsilondeoxy=interp1(nmSG,epsilondeoxySG,landa_exp);

  R_teorica=m1*epsilondeoxy+ m2*epsilonoxy+ m3*(1./(landa_exp/650)).^(1)+ m4;
%FUNCION ERROR

err = sum( (R_teorica - R_ojo).^2 );   % funcion error que se quiere minimizar en todo el intervalo espectral


%if m1<0.0;   
    err = err*10;  %rebaja las exigencias de precesión al dar una locura (S>1 no puede ser)
%end
%if m2<0.0; 
    err = err*10; %rebaja las exigencias de precesión al dar una locura (S<01 no puede ser)
%end
%if m3<0.0; 
 %   err = err*10; %rebaja las exigencias de precesión al dar una locura (S<01 no puede ser)
