function err =Funcion_ajusteHigado(start, landa_exp, R_higado, landa, muAgua, muLipidos, muHemo, muOxy,muBilis)
%result = fminsearch('Funcion_ajusteHigado', start, [], nm, ES, landa, A, B, C, D)

global cnt 

%  function err = es una funcion error entre el modelo y los datos experimentales 
%  es la funci�n que hay que minimizar mediante el algoritmo fminsearch
%  A esta funci�n el programa le pasa los datos: (start, nm0, ES, nmSG, muaoxySG, muadeoxySG, nmH2O, muaH2O,u,v)


cnt = cnt + 1;
GRAPH = 1; % 1 = ON, 0 = OFF

%par�metros de ajuste de partida.
fS=start(1);    % Fracci�n en volumen de sangre en el tejido de  higado
S=start(2);     % Saturaci�n de oxigeno en sangre(mezcla de sangre arterial y venosa)
fWL=start(3);
fL=start(4);
fB=start(5);
R=start(6);    % Radio medio de la red de capilares en  cm.
%-----Scattering-----------------------------------------------------------
a=start(7);     %Amplitud de scattering
b=start(8);     %potencia del scattering Mie
rhoM=start(9); 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MODELO DE ABSORCION DE LA HEMOGLOBINA, OXIHEMOGLOBINA, AGUA Y MELANINA  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%ESPECTROS INTERPOLADOS A ''landa_in''
muAgua=interp1(landa,muAgua,landa_exp);
muHemo=interp1(landa,muHemo,landa_exp);
muOxy=interp1(landa,muOxy,landa_exp);
muBilis=interp1(landa,muBilis,landa_exp);
muLipidos=interp1(landa,muLipidos,landa_exp);

%----------------------------------
                            

muS=fS*(S*muOxy +(1-S)*muHemo);
Cs=(1-exp(-2*R*muS))./(2*R*muS);

muSangre=fS*Cs.*(S*muOxy +(1-S)*muHemo);
muWL=fWL*(fL*muLipidos+ (1- fL)*muAgua);

muaTejido=muSangre+muWL+ 1*fB*muBilis;

landa_0=800; 
musp=a*(rhoM*(landa_exp/landa_0).^(-b)+ (1-rhoM)*(landa_exp/landa_0).^(-4));




%%%%%%%MODELO DE REFLECTANCIA%%%%%%%%%%%%%%%%%%%%%%
  r = 0.24; %0.248;%0.248;  %distancia entre las fibras cm
  n =1.38;
  ri= 0.668 + 0.0636*n + 0.710/n - 1.440/n^2; %reflexi�n 
  As = (1 + ri)/(1 - ri); % condici�n de contorno. reflectancia efectiva
  As=1;
  zo = 1./(muaTejido + musp);
  D = zo/3;
  delta = sqrt(D./muaTejido);
  r1 = sqrt(zo.^2 + r.^2);
  r2 = sqrt((zo + 4*As*D).^2 + r.^2);
  mueff = 1./delta;
  c = zo.*(mueff + 1./r1).*exp(-r1./delta)./(r1.^2);
  d = (zo + 4*As*D).*(mueff + 1./r2).*exp(-r2./delta)./(r2.^2);
  R_teorica =(musp)./(4*pi*(musp+muaTejido)).*( c + d );

%FUNCION ERROR
err = sum( (R_teorica - R_higado).^2 );   % funcion error que se quiere minimizar en todo el intervalo espectral
%err2 = sum( ((Rdifusa(kk) - ES(kk))).^2); % funcion error que se quiere minimizar en el intervalo reducido
%err  = err1*err2; % funcion error total que se quiere minnimizar en todo el intervalo espectral

%if S>1;   
 %   err = err*10;  %rebaja las exigencias de precisi�n al dar una locura (S>1 no puede ser)
%end
if S<0.0; 
    err = err*10; %rebaja las exigencias de precisi�n al dar una locura (S<01 no puede ser)
end
if fWL>1.0; 
    err = err*10; %rebaja las exigencias de preciesi�n al dar una locura (S<01 no puede ser)
end

if rhoM>1.0; 
    err = err*10; %rebaja las exigencias de preciesi�n al dar una locura (S<01 no puede ser)
end
if R<0; 
  err = err*10; %rebaja las exigencias de preciesi�n al dar una locura (S<01 no puede ser)


end


%PARA VISUALIZAR EL PROCESO DE AJUSTE EN LA PANTALLA.

if cnt<500
	QQ = (mod(cnt,10)==0);
else
	QQ = (mod(cnt,100)==0);
end

%if ( GRAPH & QQ )
if  QQ==1
    figure(2); hold off
   % set(figure(2),'position',[645    40   577   757],'color','w')
    

   plot(landa_exp, R_higado, 'ko')
    hold on
   %plot(nm0(kk), ES(kk), 'go')
   plot(landa_exp, R_teorica, 'r-','linewidth',2)

   set(gca,'fontsize',15)
   xlabel('Longitud de onda [nm]')
   ylabel('M = M_p_i_e_l/M_r_e_f')
   axis([500 1600 0 1])
    x = 520; ymax = 1; dy = .07; sz=12;
    text(x, ymax - dy, sprintf('fS =%0.3f ', fS),'fontsize',sz)
    text(x, ymax - 2*dy, sprintf('S =%0.3f ', S),'fontsize',sz)
    text(x, ymax - 3*dy, sprintf('fWL =%0.3f ', fWL),'fontsize',sz)
    text(x, ymax - 4*dy, sprintf('fL =%0.3f ', fL),'fontsize',sz)
    text(x, ymax - 5*dy, sprintf('fB =%0.3f ', fB),'fontsize',sz)
    text(x, ymax - 6*dy, sprintf('R =%0.3f ', R),'fontsize',sz)
    text(x, ymax - 7*dy, sprintf('a =%0.3f ', a),'fontsize',sz)
    text(x, ymax - 8*dy, sprintf('b =%0.3f ', b),'fontsize',sz)
    text(x, ymax - 9*dy, sprintf('rhoM =%0.3f ', rhoM),'fontsize',sz)
   % text(x, ymax - 8*dy, sprintf('nmOff = %0.3f',nmOff),'fontsize',sz)
%drawnow

end % del if


