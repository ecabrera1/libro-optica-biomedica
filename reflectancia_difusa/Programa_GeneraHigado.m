% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% PROGRAMA_GENERAL
% USA FUNCIONES: Funcion_ajusteHigado
% El programa ajusta el espectros de reflectancia difusa de la piel 
% Programa para los alumnos de �ptica biom�dica del curso 2015/16

clear all;
close all;
clc;
%-----
global cnt 

cnt = 0;
  
figure(1);
%set(figure(1),'position',[30   100   500   500],'color','w')
sz = 12;
%%%%%%%%CARGA DE FICHEROS DE ESPECTROS DE LOS CROMOFOROS RELEVANTES%%%%%%%%%%
  H=load('Tejido_Normal.dat');
 %H=load('Tejido_Tumoral.dat');
  landa_exp = H(:,1);       %longitudes de onda del espectro experimental.
  R_higado=H(:,2);          %ESPECTRO DE REFLECTANCIA EXPERIMENTAL DE LA PIEL (dividido entre 100).
%--------------------------------------------------------------------------
  plot(landa_exp, R_higado,'o-','linewidth',1)  
  set(gca,'fontsize',sz,'linewidth',2)       
  xlabel('Longitud de onda [nm]')          
  ylabel('Reflectancia')                   
  ymax = 1;
  title('Reflectancia tejido Normal');
  axis([500 1600  0 ymax]);
  axis square;

%--------------------------------------------------------------------------%
% CARGA LOS ESPECTROS DE ARCHIVO DE SANGRE Y AGUA                         % 
%--------------------------------------------------------------------------%
 A=load('muAgua.dat');
 landa=A(:,1);
 muAgua=A(:,2);

 B=load('muLipidos.dat');
 muLipidos=B(:,2);

 C=load('muHemo.dat');
 muHemo=C(:,2);

 D=load('muOxy.dat');
 muOxy=D(:,2);

 E=load('muBilis.dat');
 muBilis=E(:,2);
%----------------------------------------------------------

%selecci�n del intervalo espectral ancho
    j600=find(landa_exp==600);
    j1500=find(landa_exp==1500);
    jj = (j600:j1500);
   
%--------------------------------------------------------------------------
%                            AJUSTE DE LOS DATOS                        %
%--------------------------------------------------------------------------
    
%VALORES DE PARTIDA APROXIMADOS PARA REALIZAR EL AJUSTE
fS=0.03;%0.035;  %Fracci�n en volumen de sangre en el tejido de  higado
S= 0.37;%0.37;   %Saturaci�n de oxigeno en sangre(mezcla de sangre arterial y venosa)
fWL=0.93;
fL=0.10;
fB=0.039;
R=56*1e-4;       %Radio medio de la red de capilares en  cm.

%----- Valores `para el Scattering------------------------------------
a=14.5;          %Amplitud de scattering
b=1;             %potencia del scattering Mie
rhoM=0.25; 

%vector de valores iniciales  de los par�metros de ajuste.
start = [fS  S fWL fL fB R a b rhoM];

%%%%%FUNCI�N DE AJUSTE%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%OPTIONS = optimset('TolX',1e-12,'TolFun',1e-9,'MaxFunEvals ',1e10) ;
OPTIONS= optimset('MaxFunEvals',5000,'MaxIter',4000,'TolX',1e-6);%
result = fminsearch('Funcion_ajusteHigado', start, OPTIONS, landa_exp,R_higado, landa, muAgua, muLipidos, muHemo, muOxy,muBilis);

return
    
%Resultado del ajuste se encuentra en la variable result   anterior.
fS=result(1);    % Fracci�n en volumen de sangre en el tejido de  higado
S=result(2);     % Saturaci�n de oxigeno en sangre(mezcla de sangre arterial y venosa)
fWL=result(3);
fL=result(4);
fB=result(5);
R=result(6);    % Radio medio de la red de capilares en  cm.
%-----Scattering-----------------------------------------------------------
a=result(7);     %Amplitud de scattering
b=result(8);     %potencia del scattering Mie
rhoM=result(9); 

return
