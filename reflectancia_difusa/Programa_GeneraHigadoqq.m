% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% PROGRAMA_GENERAL
% USA FUNCIONES: Funcion_ajusteHigado
% El programa ajusta el espectros de reflectancia difusa de la piel 
% Programa para los alumnos de �ptica biom�dica del curso 2015/16

clear all;close all;clc;
  
%%%%%%%%CARGA DE FICHEROS DE ESPECTROS DE LOS CROMOFOROS RELEVANTES%%%%%%%%%%
  H=load('Tejido_Normal.dat'); %Espectro de reflectancia experimental
 %H=load('Tejido_Tumoral.dat');
  landa_exp = H(:,1);       %longitudes de onda del espectro experimental.
  R_higado=H(:,2);          %ESPECTRO DE REFLECTANCIA EXPERIMENTAL DE LA PIEL (dividido entre 100).
%--------------------------------------------------------------------------
  plot(landa_exp, R_higado,'o','linewidth',2)
  sz = 12;
  set(gca,'fontsize',sz,'linewidth',1)       
  xlabel('Longitud de onda [nm]')          
  ylabel('Reflectancia')                   
  ymax = 1;
  title('Reflectancia tejido Normal');
  axis([500 1600  0 ymax]);
  axis square;

%--------------------------------------------------------------------------%
% CARGA LOS ESPECTROS DE ARCHIVO DE SANGRE Y AGUA                         % 
%--------------------------------------------------------------------------%
 A=load('muAgua.dat');
 landa=A(:,1);
 muAgua=A(:,2);

 B=load('muLipidos.dat');
 muLipidos=B(:,2);

 C=load('muHemo.dat');
 muHemo=C(:,2);

 D=load('muOxy.dat');
 muOxy=D(:,2);

 E=load('muBilis.dat');
 muBilis=E(:,2);
%----------------------------------------------------------


   
%--------------------------------------------------------------------------
%                            AJUSTE DE LOS DATOS                        %
%--------------------------------------------------------------------------
    
%VALORES DE PARTIDA APROXIMADOS PARA REALIZAR EL AJUSTE
fS=0.03;   %Fracci�n en volumen de sangre en el tejido de  higado
S= 0.37;   %Saturaci�n de oxigeno en sangre(mezcla de sangre arterial y venosa)
fWL=0.93;  %Fracci�n en volumen del complejo agua_lipidos
fL=0.10;   %Fracci�n en volumen de l�pidos
fB=0.039;  %Fracci�n en volumen de bilirubina
R=56*1e-4; %Radio medio de la red de capilares en  cm.

%----- Valores para el Scattering------------------------------------
a=14.5;    %Amplitud de scattering
b=1;       %Potencia del scattering Mie
rhoM=0.25; %Fraccion de scattering Mie

%vector de valores iniciales  de los par�metros de ajuste.
start = [fS  S fWL fL fB R a b rhoM];

%%%%%FUNCI�N DE AJUSTE%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

OPTIONS= optimset('MaxFunEvals',5000,'MaxIter',4000,'TolX',1e-6);%
result = fminsearch('Funcion_ajusteHigadoqq', start, OPTIONS, landa_exp,R_higado, landa, muAgua, muLipidos, muHemo, muOxy,muBilis);

%------------ Dibujamos las soluciones--------------------------------
%Resultado del ajuste se encuentra en la variable result   anterior.
fS=result(1);    
S=result(2);     
fWL=result(3);
fL=result(4);
fB=result(5);
R=result(6);    
%-----Datos del Scattering-----------------------------------------------------------
a=result(7);     %Amplitud de scattering
b=result(8);     %potencia del scattering Mie
rhoM=result(9); 

%------------ESPECTROS INTERPOLADOS A ''landa_in''
muAgua=interp1(landa,muAgua,landa_exp);
muHemo=interp1(landa,muHemo,landa_exp);
muOxy=interp1(landa,muOxy,landa_exp);
muBilis=interp1(landa,muBilis,landa_exp);
muLipidos=interp1(landa,muLipidos,landa_exp);
%-----------------------------------------------                     

%--------Absorcion------------------------------
muS=fS*(S*muOxy +(1-S)*muHemo);
Cs=(1-exp(-2*R*muS))./(2*R*muS);
muSangre=fS*Cs.*(S*muOxy +(1-S)*muHemo);
muWL=fWL*(fL*muLipidos+ (1- fL)*muAgua);
muaTejido=muSangre+muWL+ 1*fB*muBilis;
%-----------Scattering---------------------------------------
landa_0=800; 
musp=a*(rhoM*(landa_exp/landa_0).^(-b)+ (1-rhoM)*(landa_exp/landa_0).^(-4));

%%--------------MODELO DE REFLECTANCIA-------------------------
  r = 0.24; %0.248;%0.248;  %distancia entre las fibras cm
  n =1.38;
  ri= 0.668 + 0.0636*n + 0.710/n - 1.440/n^2; %reflexi�n 
  As = (1 + ri)/(1 - ri); % condici�n de contorno. reflectancia efectiva
  As=1;
  zo = 1./(muaTejido + musp);
  D = zo/3;
  delta = sqrt(D./muaTejido);
  r1 = sqrt(zo.^2 + r.^2);
  r2 = sqrt((zo + 4*As*D).^2 + r.^2);
  mueff = 1./delta;
  c = zo.*(mueff + 1./r1).*exp(-r1./delta)./(r1.^2);
  d = (zo + 4*As*D).*(mueff + 1./r2).*exp(-r2./delta)./(r2.^2);
  R_teorica =(musp)./(4*pi*(musp+muaTejido)).*( c + d );
%-------------------------------------------------------------------
  plot(landa_exp, R_higado, 'go')
  hold on
  plot(landa_exp, R_teorica, 'r-','linewidth',2)
  set(gca,'fontsize',15)
  xlabel('\lambda (nm)')
  ylabel('R(\lambda)');
  title('Reflectancia tejido de higado');
  % axis([500 1600 0 1])
  x = 520; ymax = 0.8; dy = .07; sz=12;
  text(x, ymax - dy, sprintf('fS =%0.3f ', fS),'fontsize',sz)
  text(x, ymax - 2*dy, sprintf('S =%0.3f ', S),'fontsize',sz)
  text(x, ymax - 3*dy, sprintf('fWL =%0.3f ', fWL),'fontsize',sz)
  text(x, ymax - 4*dy, sprintf('fL =%0.3f ', fL),'fontsize',sz)
  text(x, ymax - 5*dy, sprintf('fB =%0.3f ', fB),'fontsize',sz)
  text(x, ymax - 6*dy, sprintf('R =%0.3f ', R),'fontsize',sz)
  text(x, ymax - 7*dy, sprintf('a =%0.3f ', a),'fontsize',sz)
  text(x, ymax - 8*dy, sprintf('b =%0.3f ', b),'fontsize',sz)
  text(x, ymax - 9*dy, sprintf('rhoM =%0.3f ', rhoM),'fontsize',sz)
  
return
