clear all; close all; clc;

A=load('I_incidente.txt');
B=load('I_transmitida_verde.txt');
C=load('Transmitancia_verde.txt');
D=load('lampara_Hidrogeno.txt');
E=load('fluorescente_clase.txt');
F=load('vela.txt');
G=load('lampara_Halogena.txt');
%--------------------------------------------
landas=A(:,1);
I_0=A(:,2);
I_T=B(:,2);
T=I_T./I_0;
Tverde=C(:,2)/100;
I_Hidrogeno=D(:,2);
F_clase=E(:,2);
I_vela=F(:,2);
I_Halogena=G(:,2);
figure
plot(landas,I_0,'r',landas,I_T,'b--','linewidth',2)
xlabel('\lambda (nm)')
ylabel('Intensidad (u.a)')
grid on

figure
plot(landas,T,'r',landas,Tverde,'b--','linewidth',2)
xlabel('\lambda (nm)')
ylabel('Transmitancia(u.a)')
axis([450 1000 0 1.1])
grid on
figure
plot(landas,I_vela,'r','linewidth',2)
xlabel('\lambda (nm)')
ylabel('I (u.a)')
axis([400 1000 0 4500])
title('Espectro de emision Vela')
grid on
figure
plot(landas,I_Halogena,'g','linewidth',2)
xlabel('\lambda (nm)')
ylabel('I (u.a)')
axis([400 1000 0 4500])
title('Espectro de emision Halógena')
grid on

figure
plot(landas,I_Hidrogeno,'r','linewidth',2)
xlabel('\lambda (nm)')
ylabel('I (u.a)')
axis([400 1000 0 4500])
title('Espectro de emision de Hidrógeno')
grid on
figure
plot(landas,F_clase,'b','linewidth',2)
xlabel('\lambda (nm)')
ylabel('I (u.a)')
axis([400 1000 0 4500])
title('Espectro de emision de tubo fluorescente')
grid on