function y =funcionError1(Valores_ini, landas, mu_sexp)

%  function err = es una funcion que calcula el 
%  error entre el modelo te�rico de mu_steorico y los datos 
%  experimentales m_sexp. Es la funci�n que hay que minimizar mediante
%  el algoritmo fminsearch.
%  A esta funci�n el programa le pasa los datos: (Valores_ini, landas, mu_sexp)



%par�metros de ajuste de partida.
 mu_500 = Valores_ini(1);
 fR = Valores_ini(2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MODELO DE SCATTERING                                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
mu_steorico=mu_500*(fR./(landas/500).^4 + (1-fR)./(landas/500).^1); 

%------------------------------------------------------------------------
%FUNCION ERROR QUE SE QUIERE MINIMIZAR
%------------------------------------------------------------------------
y = sum( (mu_steorico - mu_sexp).^2 );




